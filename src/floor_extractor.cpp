#include "floor_extractor.h"

#include <math.h>
#include <pcl/common/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/filters/impl/plane_clipper3D.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include "glog/logging.h"

namespace vehicle_detect {
namespace {
/**
 * @brief plane_clip
 * @param src_cloud
 * @param plane
 * @param negative
 * @return
 */
PointCloud::Ptr plane_clip(const PointCloud::Ptr &src_cloud, const Eigen::Vector4f &plane, bool negative)
{
  pcl::PlaneClipper3D<PointType> clipper(plane);
  pcl::PointIndices::Ptr indices(new pcl::PointIndices);

  clipper.clipPointCloud3D(*src_cloud, indices->indices);

  PointCloud::Ptr dst_cloud(new PointCloud);

  pcl::ExtractIndices<PointType> extract;
  extract.setInputCloud(src_cloud);
  extract.setIndices(indices);
  extract.setNegative(negative);
  extract.filter(*dst_cloud);

  return dst_cloud;
}

}

FloorExtractor::FloorExtractor()
{

}


int FloorExtractor::Extract(const PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl) {

  // compensate the tilt rotation
  Eigen::Matrix4f tilt_matrix = Eigen::Matrix4f::Identity();
  tilt_matrix.topLeftCorner(3, 3) = Eigen::AngleAxisf(param_.tilt_deg * M_PI / 180.0f, Eigen::Vector3f::UnitY()).toRotationMatrix();

  // filtering before RANSAC (height and normal filtering)
  PointCloud::Ptr filtered(new PointCloud);
  pcl::transformPointCloud(*input_pcl, *filtered, tilt_matrix);
  filtered = plane_clip(filtered, Eigen::Vector4f(0.0f, 0.0f, 1.0f, param_.sensor_height + param_.height_clip_range), false);
  filtered = plane_clip(filtered, Eigen::Vector4f(0.0f, 0.0f, 1.0f, param_.sensor_height - param_.height_clip_range), true);

  if (param_.use_normal_filtering)
  {
      filtered = normal_filtering(filtered);
  }

  pcl::transformPointCloud(*filtered, *output_pcl, static_cast<Eigen::Matrix4f>(tilt_matrix.inverse()));

  return 0;
}

PointCloud::Ptr FloorExtractor::normal_filtering(const PointCloud::Ptr &cloud) const
{
  pcl::NormalEstimation<PointType, pcl::Normal> ne;
  ne.setInputCloud(cloud);

  pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
  ne.setSearchMethod(tree);

  pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
  ne.setKSearch(10);
  ne.setViewPoint(0.0f, 0.0f, param_.sensor_height);
  ne.compute(*normals);

  PointCloud::Ptr filtered(new PointCloud);
  filtered->reserve(cloud->size());

  for (int i = 0; i < cloud->size(); i++)
  {
    float dot = normals->at(i).getNormalVector3fMap().normalized().dot(Eigen::Vector3f::UnitZ());
    if (std::abs(dot) > std::cos(param_.normal_filter_thresh * M_PI / 180.0))
    {
      filtered->push_back(cloud->at(i));
    }
  }

  filtered->width = filtered->size();
  filtered->height = 1;
  filtered->is_dense = false;

  return filtered;
}

}  // namespace vehicle_detect
