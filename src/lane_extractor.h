/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_LANE_EXTRATOR_H_
#define VEHICLE_DETECT_LANE_EXTRATOR_H_

#include <cstdint>
#include <vector>
#include <opencv2/core.hpp>

#include "lane2d.h"

using namespace cv;

namespace vehicle_detect {
class LaneExtractor {
 public:

//transform lane to be horizontal and clip out lane image.
  static cv::Mat FlattenLaneSection( const Mat &input_img, const LaneSection2d& section);
  static std::vector<Point2f> RestoreFlattenPoints(const std::vector<Point2f>& points, const LaneSection2d& section);
  static std::vector<RotatedRect> RestoreFlattenRects(const std::vector<Rect>& points, const LaneSection2d& section);

 private:
};

}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_EXTRATOR_H_
