#include "lane_detector.h"

#include <math.h>
#include <iterator>

#include <algorithm>

#include<opencv2/opencv.hpp>

#include "glog/logging.h"


#include "lane_section_detector.h"

using namespace cv;
using namespace std;

namespace vehicle_detect {
namespace {

}
Vehicle2dDetector::Vehicle2dDetector() {
  section_detector_.reset(new LaneSectionDetector());
}
void Vehicle2dDetector::SetParameters(size_t min_gap_width, size_t min_vehicle_width) {
  section_detector_->SetParameters(min_gap_width, min_vehicle_width);
}

int Vehicle2dDetector::Detect(const Mat &floor_img, const Mat &upground_img, const Lane2d& lane, vector<RotatedRect>& vechicles_rect) {

  vechicles_rect.clear();
  for  (const auto section : lane.sections) {
    std::vector<RotatedRect> section_vehicle_rects;
    section_detector_->Detect(floor_img, upground_img, section, section_vehicle_rects);
    copy(section_vehicle_rects.begin(), section_vehicle_rects.end(), back_inserter(vechicles_rect));
  }

  return 0;

}



}  // namespace vehicle_detect

