/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_LANE2d_H_
#define VEHICLE_DETECT_LANE2d_H_

#include <cstdint>
#include <vector>
#include <opencv2/core.hpp>



namespace vehicle_detect {
typedef struct LaneSection2d {
  cv::Point2f start;
  cv::Point2f end;
  float width;
}LaneSection2d;

typedef struct Lane2d {
  std::vector<LaneSection2d> sections;
}Lane2d;
}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_H_
