#include "libvehicle_detect.h"
#include "vehicle_detector.h"

namespace vehicle_detect{

std::shared_ptr<VehicleDetectInterface> CreateVehicleDetector()
{

    return std::shared_ptr<VehicleDetectInterface>(new VehicleDetector());
}

}// namespace vehicle_detect
