/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_BLOCK_DETECTOR_H_
#define VEHICLE_DETECT_BLOCK_DETECTOR_H_

#include <cstdint>
#include <vector>
#include <opencv2/core.hpp>


namespace vehicle_detect {
class BlockDetector {
 public:
  BlockDetector();
  ~BlockDetector(){};
  void SetParameters(size_t min_blank_width, size_t min_block_width);
  int DetectBlocks(const cv::Mat& blank_image, std::vector<cv::Rect> &  block_rects);
  int RefineBlocks(const cv::Mat& upground_image, const std::vector<cv::Rect> &  block_rects, std::vector<cv::Rect> &  entity_rects);

 private:
 
  size_t min_blank_width_;
  size_t min_block_width_;

};

}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_EXTRATOR_H_
