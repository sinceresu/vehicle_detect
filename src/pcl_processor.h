/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_PCL_PROCESSOR_H_
#define VEHICLE_DETECT_PCL_PROCESSOR_H_

#include "common.h"

namespace vehicle_detect {

class PclProcessor {
 public:
  typedef struct PclProcessParam { 
    PclProcessParam() : 
    down_sample(true),
    leaf_size(0.02f),
    remove_ceil(true),
    ceil_height(-0.2f)
    {
    };
    bool down_sample;
    float leaf_size;
    bool remove_ceil;
    float ceil_height;
  }PclProcessParam;
  
  PclProcessor();
  ~PclProcessor(){};
  void SetDownsampleParam(bool down_sample, float leaf_size) {
    param_.down_sample = down_sample;
    param_.leaf_size = leaf_size;
  }
  void SetRemoveCeilParam(bool remove_ceil, float ceil_height) {
    param_.remove_ceil = remove_ceil;
    param_.ceil_height = ceil_height;
  }

  int Process(PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl);

 private:
    void   DownsamplePcl(PointCloud::ConstPtr  input_pcl, PointCloud::Ptr output_pcl);

     /**
   * @brief filter points with non-vertical normals
   * @param cloud  input cloud
   * @return filtered cloud
   */
    PclProcessParam param_;

};

}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_EXTRATOR_H_
