#include "vehicle_detector.h"

#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/octree/octree_search.h>

#include <opencv2/opencv.hpp>

#include "bird_view_generator.h"
#include "pcl_processor.h"
#include "floor_extractor.h"
#include "lane_detector.h"

using namespace cv;
using namespace std;

namespace vehicle_detect
{

namespace
{
Point2f Convert2dPointTo3d(const Point2f &point_2d, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  Point2i min_corner_xy = Point2i(map_box.min()[0], map_box.min()[1]);
  Point2f upward_point_2d = Point2f(point_2d.x, image_size.height - point_2d.y);
  Point2f point_3d = Point2f((upward_point_2d.x + min_corner_xy.x) * voxel_size, (upward_point_2d.y + min_corner_xy.y) * voxel_size);

  return point_3d;
}

float Convert2dAngleTo3d(float angle) {
  return -angle;
}

Point2f Convert3dPointTo2d(const Point2f &point, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  Point2i min_corner_xy = Point2i(map_box.min()[0], map_box.min()[1]);
  float x_2d = point.x / voxel_size - min_corner_xy.x;
  float y_2d = point.y / voxel_size - min_corner_xy.y;
  //y axis of image direct to downward.
  return Point2f(x_2d, image_size.height - y_2d);
}


LaneSection2d Convert3dLaneSectionTo2d(const LaneSection &lane_section, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  LaneSection2d section_2d;
  section_2d.start = Convert3dPointTo2d(lane_section.start, image_size, map_box, voxel_size);
  section_2d.end = Convert3dPointTo2d(lane_section.end, image_size, map_box, voxel_size);
  section_2d.width =lane_section.width / voxel_size;   // width : odd pixels, equal width at both side of center pixel of middle line.
  return section_2d;
}
Lane2d Convert3dLaneTo2d(const Lane &lane, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  Lane2d lane_2d;
  for (const auto &lane_section : lane.sections)
  {
    lane_2d.sections.push_back(Convert3dLaneSectionTo2d(lane_section, image_size, map_box, voxel_size));
  }
  return lane_2d;
}

vector<Lane2d> Convert3dLanesTo2d(const vector<Lane> &lanes, const Size &image_size, const Eigen::AlignedBox3i &map_box, float voxel_size)
{
  vector<Lane2d> lane_2ds;
  for (const Lane &lane : lanes)
  {
    lane_2ds.push_back(Convert3dLaneTo2d(lane, image_size, map_box, voxel_size));
    // Point2f start_offst  = lane.start
  }

  return lane_2ds;
}

Mat FilterFloor(const Mat &input_img)
{
  int erode_pixels = 1;
  Mat element = getStructuringElement(MORPH_RECT,
                                      Size(2 * erode_pixels + 1, 2 * erode_pixels + 1),
                                      Point(erode_pixels, erode_pixels));
  Mat eroded = Mat();
  erode(input_img, eroded, element);
  // int dilation_size  = 2;
  // element = getStructuringElement( MORPH_RECT,
  //                      Size( 2*dilation_size + 1, 2*dilation_size+1 ),
  //                      Point( dilation_size, dilation_size ) );
  Mat output_image;
  dilate(eroded, output_image, element);
  return output_image;
}

Mat FilterUpground(const Mat &input_img)
{
  Mat upground_mask;
  bitwise_not(input_img, upground_mask);
  return FilterFloor(upground_mask);
}

Mat Align2dMap(const Mat &floor_img, const Eigen::AlignedBox3i floor_box, const Mat &raw_img, const Eigen::AlignedBox3i raw_box)
{

  int x_offset = floor_box.min()[0] - raw_box.min()[0];
  int y_offset = raw_box.max()[1] - floor_box.max()[1];
  // assert(x_offset >= 0 && y_offset >= 0);
  Mat clip_raw_img = cv::Mat::zeros( floor_img.rows, floor_img.cols, floor_img.type());
  int x_src = max(x_offset, 0);
  int y_src = max(y_offset, 0);
  int x_dst = max(-x_offset, 0);
  int y_dst = max(-y_offset, 0);
  int width = min(floor_img.cols - x_dst, raw_img.cols - x_src);
  int height = min(floor_img.rows - y_dst, raw_img.rows - y_src);
  raw_img(Rect(x_src, y_src, width, height)).copyTo(clip_raw_img(Rect(x_dst, y_dst, width, height)) );
  // clip_raw_img(Rect(x_dst, y_dst, width, height)) = raw_img(Rect(x_src, y_src, width, height)).clone();
  return clip_raw_img;

}
PointCloud::Ptr ExtractUpground(PointCloud::ConstPtr raw_pcl, PointCloud::ConstPtr floor_pcl, float min_height)
{
  PointCloud::Ptr output_pcl(new PointCloud());
  pcl::octree::OctreePointCloudSearch<PointType> octree(min_height);
  octree.setInputCloud(floor_pcl);
  octree.addPointsFromInputCloud();

  std::vector<int> pointIdxVec;
  for (const PointType &searchPoint : raw_pcl->points)
  {
    if (!octree.voxelSearch(searchPoint, pointIdxVec))
    {
      output_pcl->push_back(searchPoint);
    }
  }
  return output_pcl;
}

}

  VehicleDetector::VehicleDetector()
  {
    pcl_processor_ = make_shared<PclProcessor>();
    floor_extractor_ = make_shared<FloorExtractor>();
    vehicle_2d_detector_ = make_shared<Vehicle2dDetector>();
  }

  int VehicleDetector::SetParameter(const VehicleDetectParam &param)
  {
    parameter_ = param;
    return 0;
  }

  int VehicleDetector::SetPointCloudMap(const string &pointcloud_filepath)
  {
    map_filepath_ = pointcloud_filepath;
    return 0;
  }

  int VehicleDetector::DetectVehicles(const vector<Lane> &lanes, DetectResult &detect_result)
  {
    if (map_filepath_.empty())
    {
      return -1;
    }
    PointCloud::Ptr raw_pcl(new PointCloud());
    // std::string filepath = "/home/sujin/ScannerWorkspace/a/a.pcd";
    if (pcl::io::loadPCDFile(map_filepath_, *raw_pcl) == -1)
      return -1;

    PointCloud::Ptr processed(new PointCloud());
    pcl_processor_->Process(raw_pcl, processed);

    FloorExtractor::FloorExtractParam param;
    param.sensor_height = parameter_.sensor_height;
    param.height_clip_range = parameter_.height_clip_range;
    param.use_normal_filtering = parameter_.use_normal_filtering;
    param.normal_filter_thresh = 10;
    floor_extractor_->SetParam(param);

    PointCloud::Ptr floor_pcl(new PointCloud());

    floor_extractor_->Extract(processed, floor_pcl);
    PointCloud::Ptr upground_pcl = ExtractUpground(processed, floor_pcl, 0.2);
    pcl::io::savePCDFileBinary("upground.pcd", *upground_pcl);

    std::string map_2d_name = "map2d.png";
    auto map_box = Convert(processed, parameter_.voxel_size, false, map_2d_name);

    std::string floor_img_name = "floor.png";
    auto floor_box = Convert(floor_pcl, parameter_.voxel_size, true, floor_img_name);
    Mat floor_image = imread(floor_img_name, IMREAD_GRAYSCALE);
    Mat filtered_floor_image = FilterFloor(floor_image);
    imwrite("filtered_floor.png", filtered_floor_image);

    // std::string raw_img_name = "raw.png";
    // auto raw_box = Convert(downsampled, parameter_.voxel_size, raw_img_name);

    // Mat raw_image =  imread(raw_img_name,  IMREAD_GRAYSCALE);
    // Mat filtered_raw_image = FilterImage(raw_image);
    // imwrite(raw_img_name, filtered_raw_image);

    std::string upground_img_name = "upground.png";
    auto upground_box = Convert(upground_pcl, parameter_.voxel_size, true, upground_img_name);

    Mat up_image = imread(upground_img_name, IMREAD_GRAYSCALE);
    Mat filtered_up_image = FilterUpground(up_image);
    Mat upground_image = Align2dMap(filtered_floor_image, floor_box, filtered_up_image, upground_box);
    imwrite(" filtered_upground.png", upground_image);

    vector<Lane2d> image_lanes = Convert3dLanesTo2d(lanes, floor_image.size(), floor_box, parameter_.voxel_size);

    vehicle_2d_detector_->SetParameters((int)round(parameter_.min_gap_distance / parameter_.voxel_size), (int)round(parameter_.min_vehicle_width / parameter_.voxel_size));

    detect_result.lanes.clear();
    for (const Lane2d &image_lane : image_lanes)
    {
      std::vector<RotatedRect> image_vehicle_rects;
      vehicle_2d_detector_->Detect(filtered_floor_image, upground_image, image_lane, image_vehicle_rects);

      std::vector<VehicleBlock> lane_vehicle_blocks;
      for (const auto &image_vehicle_rect : image_vehicle_rects)
      {
        Point2f center_3d = Convert2dPointTo3d(image_vehicle_rect.center, floor_image.size(), floor_box, parameter_.voxel_size);
        RotatedRect3d rect_3d;
        rect_3d.center = Point3f(center_3d.x, center_3d.y, -parameter_.sensor_height);
        rect_3d.size = cv::Size2f(image_vehicle_rect.size.width *  parameter_.voxel_size, image_vehicle_rect.size.height * parameter_.voxel_size);
        rect_3d.angle = Convert2dAngleTo3d(image_vehicle_rect.angle);
        
        VehicleBlock vehicle_block;
        vehicle_block.bottom = rect_3d;
        vehicle_block.height = parameter_.sensor_height;
        // Point2f center_3d = Point2f((image_vehicle_center.x + 0.5f) * parameter_.voxel_size, (image_vehicle_center.y + 0.5f) * parameter_.voxel_size);
        lane_vehicle_blocks.push_back(vehicle_block);
      }
      detect_result.lanes.push_back(lane_vehicle_blocks);
    }

    return 0;
  }

} // namespace vehicle_detect
