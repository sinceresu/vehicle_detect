#include "lane_section_detector.h"

#include <math.h>
#include<opencv2/opencv.hpp>

#include "glog/logging.h"


#include "lane_extractor.h"
#include "block_detector.h"

using namespace cv;

namespace vehicle_detect {
namespace {

}
LaneSectionDetector::LaneSectionDetector() {
  block_counter_.reset(new BlockDetector());
}
void LaneSectionDetector::SetParameters(size_t min_gap_width, size_t min_vehicle_width) {
  block_counter_->SetParameters(min_gap_width, min_vehicle_width);
}

int LaneSectionDetector::Detect(const Mat &floor_img, const Mat &upground_img,  const LaneSection2d& section, std::vector<RotatedRect >& vechicles_rect) {

  auto flattened_floor_img = LaneExtractor::FlattenLaneSection(floor_img, section);
  imwrite("flattened_floor.png", flattened_floor_img);
  auto flattened_upground_img = LaneExtractor::FlattenLaneSection(upground_img, section);
  imwrite("flattened_upground.png", flattened_upground_img);

   std::vector<Rect> flatten_blocks_rect;
  block_counter_->DetectBlocks(flattened_floor_img, flatten_blocks_rect);
   std::vector<Rect> flatten_vechicles_rect;
  block_counter_->RefineBlocks(flattened_upground_img, flatten_blocks_rect, flatten_vechicles_rect);

  vechicles_rect =  LaneExtractor::RestoreFlattenRects(flatten_vechicles_rect, section);

  return 0;

}



}  // namespace vehicle_detect

