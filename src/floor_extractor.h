/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_FLOOR_EXTRACTOR_H_
#define VEHICLE_DETECT_FLOOR_EXTRACTOR_H_

#include <vector>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "common.h"

namespace vehicle_detect {


class FloorExtractor {
 public:
  typedef struct FloorExtractParam { 
    FloorExtractParam() : 
    tilt_deg(0.f),
    sensor_height(1.85),
    height_clip_range(0.2f),
    use_normal_filtering(true),
    normal_filter_thresh(20)
    {
    };
    float tilt_deg;
    float sensor_height;
    float height_clip_range;
    bool use_normal_filtering;
    float normal_filter_thresh;
  }FloorExtractParam;
  
  FloorExtractor();
  ~FloorExtractor(){};
  void SetParam(FloorExtractParam param) {
    param_ = param;
  }
  int Extract(const PointCloud::ConstPtr input_pcl, PointCloud::Ptr output_pcl);

 private:
     /**
   * @brief filter points with non-vertical normals
   * @param cloud  input cloud
   * @return filtered cloud
   */
    PointCloud::Ptr normal_filtering(const PointCloud::Ptr &cloud) const;
    FloorExtractParam param_;

};

}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_EXTRATOR_H_
