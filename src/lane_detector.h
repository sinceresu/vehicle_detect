/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_LANE_DETECTOR_H_
#define VEHICLE_DETECT_LANE_DETECTOR_H_

#include <memory>
#include <vector>
#include <opencv2/core.hpp>

#include "lane2d.h"

namespace vehicle_detect {
class LaneSectionDetector;

class Vehicle2dDetector {
public:
  Vehicle2dDetector();
  ~Vehicle2dDetector(){};

  void SetParameters(size_t min_gap_width, size_t min_vehicle_width);

  int  Detect(const cv::Mat &floor_img, const cv::Mat &upground_img,  const Lane2d& lane, std::vector<cv::RotatedRect>& vechicles_center);
private:
  std::shared_ptr<LaneSectionDetector> section_detector_;
};

}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_SECTION_DETECTOR_H_
