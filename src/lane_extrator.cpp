#include "lane_extractor.h"
#include <iostream>

#include <math.h>
#include <tuple>

#include<opencv2/opencv.hpp>

#include "glog/logging.h"

using namespace cv;
using namespace std;

namespace vehicle_detect {
namespace {
  
std::tuple<Mat,Mat> GetAffineMat(const LaneSection2d& section ) {
  // Point2i section.start = section.start.x < section.end.x ? section.start : section.end;
  // Point2i section.end = section.start.x < section.end.x ? section.end : section.start; 

  float angle = atan2(section.end.y - section.start.y, section.end.x - section.start.x) * 180 / M_PI;
  // rotate line to be horizontal;
  Mat rotation_matix = getRotationMatrix2D(section.start, angle, 1.0);

  //move left top corner of lane to origin of image.
	double warp_values[] = { 1.0, 0.0, -(double)section.start.x, 0.0, 1.0, -(double)(section.start.y -  section.width / 2)};
	Mat translation_matrix = Mat(2, 3, CV_64F, warp_values);
  return make_tuple(translation_matrix.clone(), rotation_matix.clone());
}
float GetLaneAngle(const LaneSection2d& section ) {
  return atan2(section.end.y - section.start.y, section.end.x - section.start.x) * 180 / M_PI;
}
}
Mat LaneExtractor::FlattenLaneSection( const Mat &input_img, const LaneSection2d& section) {
  // rotate line to be horizontal;
  Mat rotation_matix;
	Mat translation_matrix;
  tie(translation_matrix, rotation_matix) = GetAffineMat(section);

  Mat rotated_image;
  warpAffine(input_img, rotated_image, rotation_matix, input_img.size());
  // imwrite("rotated.png", rotated_image);
  Mat tranlated_image;
  warpAffine(rotated_image, tranlated_image, translation_matrix, rotated_image.size());
  // imwrite("translated.png", tranlated_image);

  // cout << rotation_matix << endl;
 imwrite("flattened.png", tranlated_image);
  // rotated_image(Rect(section.start.x, section.start.y - to_top, section_length, to_top + to_bottom)).copyTo(extracted_image(Rect(0, lane_width / 2 - to_top, section_length, to_top + to_bottom)));
  int section_width = static_cast<int>(ceil(norm(section.end - section.start))) ;
  int section_height = static_cast<int>(ceil(section.width)) ;
  return  tranlated_image(Rect(0, 0, section_width, section_height)).clone();

}

std::vector<RotatedRect> LaneExtractor::RestoreFlattenRects(const std::vector<Rect>& flatten_rects, const LaneSection2d& section) {
  std::vector<Point2f> points;
  for(const auto& rect : flatten_rects) {
    points.push_back(Point2f(rect.x + (rect.width - 1)  / 2.0f, rect.y + ( rect.height - 1) / 2.0f));
  }
  std::vector<Point2f> transformed_points = LaneExtractor::RestoreFlattenPoints(points, section);
  float angle = GetLaneAngle(section);

  std::vector<RotatedRect> restored_rects;
  for(int i = 0; i < flatten_rects.size(); i++) {
    RotatedRect restored_rect;
    restored_rect.center = transformed_points[i];
    restored_rect.size = Size2f(flatten_rects[i].width, flatten_rects[i].height);
    restored_rect.angle = angle;

    restored_rects.push_back(restored_rect);
  }
  return restored_rects;
}

std::vector<Point2f> LaneExtractor::RestoreFlattenPoints(const std::vector<Point2f>& points, const LaneSection2d& section) {

  std::vector<Point2f> restored_points;
  // rotate line to be horizontal;
  Mat rotation_matix;
	Mat translation_matrix;
  tie(translation_matrix, rotation_matix) = GetAffineMat(section);

  Mat revert_translation_matrix;
  invertAffineTransform(translation_matrix, revert_translation_matrix);

  std::vector<Point2f> translated_points;
  transform(points, translated_points, revert_translation_matrix);

  Mat revert_rotate_matrix;
  invertAffineTransform(rotation_matix, revert_rotate_matrix);

  // cout << rotation_matix << endl;
  // cout << revert_rotate_matrix << endl;

  transform(translated_points, restored_points, revert_rotate_matrix);

  return restored_points;
}

}  // namespace vehicle_detect
