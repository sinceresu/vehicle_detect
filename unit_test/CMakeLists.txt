cmake_minimum_required(VERSION 3.0.2)
project(unit_test)

find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(PCL REQUIRED )
find_library(CAIRO_LIBRARIES cairo)
find_package(Boost REQUIRED )

set(test_floor_extractor_srcs
  test_floor_extractor.cpp
  ../src/floor_extractor.cpp

)
add_executable(test_floor_extractor
  ${test_floor_extractor_srcs}
)

target_include_directories(test_floor_extractor
 SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include )
target_include_directories(test_floor_extractor
 SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/src )
# PCL
target_include_directories(test_floor_extractor
 SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

# Eigen
target_include_directories(test_floor_extractor
 SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})

target_include_directories(test_floor_extractor
 SYSTEM PUBLIC
  "${Boost_INCLUDE_DIRS}")
target_link_libraries(test_floor_extractor
 PUBLIC ${Boost_LIBRARIES})


target_link_libraries(test_floor_extractor
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
  ${EIGEN3_LIBRARIES}
  gflags
  glog
)

set(test_bird_view_srcs
  test_bird_view.cpp
  ../src/bird_view_generator.cpp 
  ../src/file_writer.cpp 
  ../src/image.cpp 
  ../src/xray_points_processor.cpp 

)
add_executable(test_bird_view
  ${test_bird_view_srcs}
)

target_include_directories(test_bird_view
 SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include )
target_include_directories(test_bird_view
 SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/src )
# PCL
target_include_directories(test_bird_view
 SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

# Eigen
target_include_directories(test_bird_view
 SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})

target_include_directories(test_bird_view
 SYSTEM PUBLIC
  "${Boost_INCLUDE_DIRS}")


target_link_libraries(test_bird_view
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
  ${EIGEN3_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CAIRO_LIBRARIES}
  gflags
  glog
)


set(test_section_detector_srcs
  test_section_detector.cpp
  ../src/lane_section_detector.cpp 
  ../src/lane_extrator.cpp 
  ../src/block_counter.cpp 

)
add_executable(test_section_detector
  ${test_section_detector_srcs}
)

target_include_directories(test_section_detector
 SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include )
target_include_directories(test_section_detector
 SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/src )
# PCL
target_include_directories(test_section_detector
 SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})

# Eigen
target_include_directories(test_section_detector
 SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})

target_include_directories(test_section_detector
 SYSTEM PUBLIC
  "${Boost_INCLUDE_DIRS}")


target_link_libraries(test_section_detector
  ${OpenCV_LIBS}
  ${PCL_LIBRARIES}
  ${EIGEN3_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CAIRO_LIBRARIES}
  gflags
  glog
)