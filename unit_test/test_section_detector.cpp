#include <algorithm>
#include <fstream>
#include <iostream>

#include<opencv2/opencv.hpp>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "lane_section_detector.h"


DEFINE_double(sensor_height, 2.0,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
         
DEFINE_double(height_clip_range, 0.2,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");   
DEFINE_bool(use_normal_filtering, false,
            "Activates the collection of runtime metrics. If activated, the "
            "metrics can be accessed via a ROS service.");
DEFINE_string(input_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");                    
DEFINE_string(output_filename, "/camera/image_right",
              "If non-empty, filename of a .pbstream file to load, containing "
              "a saved SLAM state.");


using namespace std;
using namespace cv;

namespace vehicle_detect {
void Run(int argc, char** argv) {
  LaneSectionDetector section_detector;

  section_detector.SetParameters(5,  10);

  Mat input_image =  imread(FLAGS_input_filename,  IMREAD_GRAYSCALE);

  LaneSection2d section;
  section.start = Point2f(50, 304);
  section.end = Point2f(143, 295);
  section.width = 5;

  std::vector<cv::Point2f> center_of_vehicles;
  
  section_detector.Detect(input_image, section, center_of_vehicles);



  cout << "vehicles : " << center_of_vehicles.size() << endl; 
  for (const auto & center : center_of_vehicles) {
    cout <<  "centers: " << center << endl;

  }


}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  vehicle_detect::Run(argc, argv);

}


