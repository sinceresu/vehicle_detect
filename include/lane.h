/*
 * Copyright 2017 The vehicle_detect Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VEHICLE_DETECT_LANE_H_
#define VEHICLE_DETECT_LANE_H_

#include <cstdint>
#include <vector>
#include <opencv2/core.hpp>



namespace vehicle_detect {
typedef struct LineString {
  std::vector<cv::Point2f> through_points;
}LineString;

typedef struct Lane {
  LineString middle_line;
  float width; 
}Lane;
}  // namespace vehicle_detect

#endif  // VEHICLE_DETECT_LANE_H_
