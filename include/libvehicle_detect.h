#ifndef VEHICLE_DETECT_LIBVEHICAL_DETECT_H
#define VEHICLE_DETECT__LIBVEHICAL_DETECT_H


#if (defined _WIN32 || defined WINCE || defined __CYGWIN__) && defined  LIBmap_scanner_EXPORTS
#define LIBmap_scanner_API __declspec(dllexport)
#elif defined __GNUC__ && __GNUC__ >= 4
#  define LIBmap_scanner_API __attribute__ ((visibility ("default")))
#else
#define LIBmap_scanner_API
#endif

#include <memory>
#include <stdint.h>
#include <memory>

namespace vehicle_detect{

class VehicleDetectInterface;


  /******************************************************************************
  * \fn CreateMapBuilderr
  * Create Time: 2020/06/02
  * Description: -
  *   创建一个新的扫描组件实例
  *
  * \param type 输入参数
  * 		扫描组件实例类型
  *
  * \return
  * 		扫描组件实例
  *
  * \note 
  *******************************************************************************/
std::shared_ptr<VehicleDetectInterface> CreateVehicleDetector();

}// namespace vehicle_detect

#endif  
