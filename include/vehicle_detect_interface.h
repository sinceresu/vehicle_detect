#ifndef VEHICLE_DETECT_VEHICLE_DETECT_INTERFACE_H_
#define VEHICLE_DETECT_VEHICLE_DETECT_INTERFACE_H_

#include <vector>
#include <string>
#include <memory>

#include <opencv2/core.hpp>


namespace vehicle_detect 
{

typedef struct LaneSection {
  cv::Point2f start;
  cv::Point2f end;
  float width; 
}LaneSection;

typedef struct Lane {
  std::vector<LaneSection> sections;
}Lane;

typedef struct RotatedRect3d {
  cv::Point3f center;
  cv::Size2f size;
  float angle;
}RotatedRect3d;

typedef struct VehicleBlock {
  RotatedRect3d bottom;
  float height;
}VehicleBlock;

typedef std::vector<VehicleBlock> LaneResult;

typedef struct DetectResult {
  std::vector<LaneResult> lanes;
}DetectResult;



class VehicleDetectInterface
{
public:
      
  typedef struct VehicleDetectParam { 
    float sensor_height;  //lidar height.
    float height_clip_range;
    bool use_normal_filtering;  //lidar height.
    float voxel_size;  //lidar height.
    float min_gap_distance;  //minimum distance between two vehicles.
    float min_vehicle_width;  //minimum width of a vehicle.

  }VehicleDetectParam;


  virtual ~VehicleDetectInterface(){};

    /******************************************************************************
  * \fn VehicleDetectInterface.SetParam
  * Create Time: 2020/06/02
  * Description: -
  *    设置拼图配置文件
  *
  * \param param 输入参数
  * 		urdf 文件路径
  *
  * \param config_file_name 输入参数
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual int SetParameter(const VehicleDetectParam & param) = 0 ;

  virtual int  SetPointCloudMap(const std::string & pointcloud_filepath) = 0 ;

  virtual int DetectVehicles(const std::vector<Lane>& lanes, DetectResult & detect_result) = 0 ;
  
  private:
    std::string map_filepath;
};
}// namespace vehicle_detect

#endif  
